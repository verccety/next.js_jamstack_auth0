import Head from 'next/head';
import Navbar from '../components/Navbar';
import { useContext, useEffect } from 'react';
import Todo from '../components/Todo';
import { table, minifyRecords } from '../utils/Airtable';
import { TodosContext } from '../contexts/TodosContext';
import { useUser, getSession } from '@auth0/nextjs-auth0';
import TodoForm from '../components/TodoForm';

export default function Home({ initialTodos }) {
  const { todos, setTodos } = useContext(TodosContext);

  const { user, error, isLoading } = useUser();

  useEffect(() => {
    setTodos(initialTodos);
  }, []);
  return (
    <div>
      <Head>
        <title>Authenticated TODO App</title>
        <link rel='icon' href='/favicon.ico' />
      </Head>
      <Navbar user={user} />

      <main>
        {user && (
          <>
            <h1 className='text-2xl text-center mb-4'>My Todos</h1>
            <TodoForm />
            <ul>{todos && todos.map((todo) => <Todo key={todo.id} todo={todo} />)}</ul>{' '}
          </>
        )}
        {!user && <p>You should log in to save your TODOs</p>}
      </main>
    </div>
  );
}

export async function getServerSideProps(context) {
  const session = getSession(context.req, context.res);
  let todos = [];
  try {
    if (session?.user) {
      todos = await table.select({ filterByFormula: `userId = '${session.user.sub}'` }).firstPage();
    }
    return {
      props: {
        initialTodos: minifyRecords(todos),
      },
    };
  } catch (error) {
    console.error(error);
    // ? you can't pass 'undefined' in JSON (use null instead)
    return {
      props: {
        error: 'Something went wrong',
      },
    };
  }
}
