Built using [Auth0 Guide](https://www.youtube.com/playlist?list=PLZ14qQz3cfJJOcbbVi_nVEPqC2334LLMz) 
## Getting Started

First, run the development server:

```bash
npm run dev
# or
yarn dev
```

Visit [Deployment Build](https://next-js-jamstack-auth0.vercel.app/) with your browser to see the result.