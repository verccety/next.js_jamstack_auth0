import { table, getMinifiedRecord } from '../../utils/Airtable';
import { getSession } from '@auth0/nextjs-auth0';
import OwnsRecord from '../../utils/middleware/OwnsRecord';

export default OwnsRecord(async (req, res) => {
  const { id } = req.body;
  const { user } = getSession(req);

  try {
    const deletedRecords = await table.destroy([id]);

    res.statusCode = 200;
    res.json(getMinifiedRecord(deletedRecords[0]));
  } catch (error) {
    console.error(error);
    res.statusCode = 500;
    res.json({ msg: 'Something went wrong' });
  }
});
