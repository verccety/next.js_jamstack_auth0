module.exports = {
    printWidth: 250,
    singleQuote: true,
    semi: true,
    jsxSingleQuote: true,
    jsxBracketSameLine: false,
  };
  