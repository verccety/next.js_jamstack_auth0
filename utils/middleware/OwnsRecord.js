import { table, getMinifiedRecord } from '../../utils/Airtable';
import { withApiAuthRequired, getSession } from '@auth0/nextjs-auth0';

// prevents user to manipulate records created by another user
const ownsRecord = (handler) => withApiAuthRequired(async (req, res) => {
  const { user } = getSession(req);

  const { id } = req.body;

  try {
    const existingRecord = await table.find(id);

    if (!existingRecord || user.sub !== existingRecord.fields.userId) {
      res.statusCode = 404;

      // specifically ambiguous error msg
      return res.json({ msg: 'Record not found' });
    }

    req.record = existingRecord;
    return handler(req, res);
  } catch (error) {
    console.error(error);
    res.statusCode = 500;
    return res.json({ msg: 'Something went wrong' });
  }
});

export default ownsRecord;
