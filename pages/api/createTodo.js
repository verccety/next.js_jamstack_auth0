import { table } from '../../utils/Airtable';
import { withApiAuthRequired, getSession } from '@auth0/nextjs-auth0';

export default withApiAuthRequired(async (req, res) => {
  const { description } = req.body;
  const { user } =  getSession(req);

  try {
    // array will be returned, so recordS
    const createdRecords = await table.create([{ fields: { description, userId: user.sub } }]);
    const createdRecord = {
      id: createdRecords[0],
      fields: createdRecords[0].fields,
    };

    res.statusCode = 200;
    res.json(createdRecord);
  } catch (error) {
    console.error(error);
    res.statusCode = 500;
    res.json({ msg: 'Something went wrong' });
  }
});
